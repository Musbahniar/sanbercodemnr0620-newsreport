import React, { useState } from 'react';
import {
  Image,
  StyleSheet,
  Dimensions,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import { TextInput, Button } from 'react-native-paper';

const W = Dimensions.get('window').width;

import Block from '../../components/block';
import TextView from '../../components/textview';
import Colors from '../../components/color';

const styles = StyleSheet.create({
  img: {
    width: '100%',
    height: 300,
  },
  reporter: {
    position: 'absolute',
    top: 100,
    left: 60,
  },
  wrapperimage: {
    position: 'absolute',
    bottom: 0,

    alignSelf: 'center',
    width: W,
    height: 300,
  },
  bg: {
    position: 'absolute',
    width: 1000,
    height: 1000,
    top: -(930 - W / 2),
    alignSelf: 'center',
    borderRadius: 1000,
    overflow: 'hidden',
  },
  containerHeader: {
    position: 'relative',
  },
});

const LoginScreen = () => {
  const navigation = useNavigation();

  const [form, setForm] = useState({
    email: '',
    password: ''
  });


  return (
    <ScrollView style={{ flex: 1 }}>
      <Block block color="#fafafa">
        <Block height={300} color={Colors.blue} style={styles.bg}>
          <Block style={styles.wrapperimage}>
            <Image
              style={styles.reporter}
              source={require('../../assets/reporter.png')}
            />
          </Block>
        </Block>
        <Block style={styles.containerHeader}>
          <Image style={styles.img} source={require('../../assets/bintang.png')} />
        </Block>
        <Block block padding={10}>
          <TextView h3 style={{ textAlign: 'center' }} color={Colors.blue1}>Welcome back!</TextView>
        </Block>
        <Block padding={50}>
          <Block justifyContent="center">
            <Block>
              <TextInput
                style={{ backgroundColor: "transparent" }} 
                label="Email" 
                value={form.email}
                onChangeText={email => setForm(email)}
              />
              <TextInput
                label='Password' 
                style={{ marginTop: 8, backgroundColor: "transparent" }}
                value={form.password}
                onChangeText={password => setForm(password)}
              />
              <Button 
                mode="contained" 
                style={{ marginTop: 18 }}
                onPress={() => navigation.navigate('home')}>
              Login</Button>
            </Block>
          </Block>
        </Block>
      </Block>
    </ScrollView>
  );
};

export default LoginScreen;