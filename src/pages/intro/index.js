import React from 'react';
import { StyleSheet, View, Text, Button, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import LoginScreen from '../login';

export default class Intro extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          showRealApp: false,
      };
  }
  _onDone = () => {
      this.setState({ showRealApp: true });
  };
  _onSkip = () => {
      this.setState({ showRealApp: true });
  };

  _renderItem = ({ item }) => {
    return (
      <View style={[
        styles.slide,
        {
          backgroundColor: item.backgroundColor,
        },
      ]}>
        <Text style={styles.title}>{item.title}</Text>
        <Image style={styles.image} source={item.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  }


  // _signInAsync = async () => {
  //     await AsyncStorage.setItem('userToken', 'abc');
  //     this.props.navigation.navigate('App');
  // };

  render() {
      if (this.state.showRealApp) {
          return (
              <LoginScreen />

          );
      } else {
          return (
              <AppIntroSlider
              data={slides}
              renderItem={this._renderItem}
              onDone={this._onDone}
              showSkipButton={true}
              onSkip={this._onSkip}
            />
          );
      }
  }
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
  },
  image: {
    width: 200,
    height: 200,
    marginVertical: 22,
  },
  text: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 20,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  },
});


const slides = [
  {
      key: 's1',
      text: 'Kami menyediakan sistem informasi secara gratis kepada warganet  yang terhubung langsung dengan sanbercodeNews',
      title: 'Ketersediaan Informasi Yang Cepat',
      image: require("../../../src/assets/intro-realtime.png"),
      backgroundColor: '#3B00AB',
    },
    {
      key: 's2',
      text: 'Kami menjamin keamanan data dan keabsahan dari informasi yang kami terima, bahkan akan meverifikasi bahwa berita tersebut bukanlah HOAX!',
      title: 'Informasi Cepat dan Terpercaya',
      titleStyle: styles.title,
      image: require("../../../src/assets/intro-transaksi-aman.png"),
      imageStyle: styles.image,
      backgroundColor: '#FF7500',
    },
];